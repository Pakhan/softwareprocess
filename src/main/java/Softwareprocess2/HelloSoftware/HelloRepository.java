package Softwareprocess2.HelloSoftware;

import org.springframework.data.jpa.repository.JpaRepository;

public interface HelloRepository extends JpaRepository<Hello, Integer>{
     
}