package Softwareprocess2.HelloSoftware;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController   
public class HelloController {
    
    @Autowired
    HelloService helloService;

    @GetMapping("/text/{id}")
    public String getTextById(@PathVariable("id")int id){
        Hello text_id = helloService.getTextById(id);
        return "Hello "+text_id.getText();
        
    }

    @GetMapping("/texts")
    public ResponseEntity<List<Hello>> getAllText(){
        List<Hello> texts = helloService.getAllText();
        return new ResponseEntity<List<Hello>>(texts, HttpStatus.OK);
    }
}