package Softwareprocess2.HelloSoftware;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.ResourceAccessException;

@Service
public class HelloService {
    @Autowired
    HelloRepository helloRepository;

    public Hello getTextById(int id){
        return helloRepository.findById(id).orElseThrow(()-> new ResourceAccessException("Not Found Id: " + id));
    }

    public List<Hello> getAllText(){
        return helloRepository.findAll();
    }
}