package Softwareprocess2.HelloSoftware;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

@Entity
@Table(name="hello")
public class Hello {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @NotBlank
    private String text;

    public Hello(){
        
    }

    public Hello(String text){
        this.text=text;
    }

    public String getText(){
        return text;
    }

    public void setText(String text){
        this.text=text;
    }

    // public String toString(){
    //     return "Hello1"+this.text;
    // }
}
